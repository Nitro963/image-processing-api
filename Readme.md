### Image super-resolution API

an API for super resolution technology implemented in python using Django REST Framework and Tensorflow

the used model is EDSR(Enhanced Deep Super Resolution):
[1] Bee Lim, Sanghyun Son, Heewon Kim, Seungjun Nah, and Kyoung Mu Lee, "Enhanced Deep Residual Networks for Single Image Super-Resolution," 2nd NTIRE: New Trends in Image Restoration and Enhancement workshop and challenge on image super-resolution in conjunction with CVPR 2017. [PDF](http://openaccess.thecvf.com/content_cvpr_2017_workshops/w12/papers/Lim_Enhanced_Deep_Residual_CVPR_2017_paper.pdf) [arxiv](https://arxiv.org/abs/1707.02921)

We used [DIV2K](http://www.vision.ee.ethz.ch/~timofter/publications/Agustsson-CVPRW-2017.pdf) dataset to train the model

the final weights and training logs is publicly available [Training Logs](https://drive.google.com/drive/folders/1zUjW5BIqTyRLet96tMOXcMufPxM3WP2o?usp=sharing)