import os
import threading
import multiprocessing as mp
from queue import Queue
from time import sleep

import numpy as np
import tensorflow as tf

from tensorflow.keras.layers import Add, Conv2D, Input, Lambda
from tensorflow.keras.models import Model
from PIL import Image
from profiles_api import models
from processing_apis_project.settings import MEDIA_ROOT

DIV2K_RGB_MEAN = np.array([0.4488, 0.4371, 0.4040]) * 255


def edsr(scale, num_filters=64, num_res_blocks=16, res_block_scaling=None):
    """Creates an EDSR model."""
    x_in = Input(shape=(None, None, 3))
    x = Lambda(normalize)(x_in)

    x = b = Conv2D(num_filters, 3, padding='same')(x)
    for i in range(num_res_blocks):
        b = res_block(b, num_filters, res_block_scaling)
    b = Conv2D(num_filters, 3, padding='same')(b)
    x = Add()([x, b])

    x = upsample(x, scale, num_filters)
    x = Conv2D(3, 3, padding='same')(x)

    x = Lambda(denormalize)(x)
    return Model(x_in, x, name="edsr")


def res_block(x_in, filters, scaling):
    """Creates an EDSR residual block."""
    x = Conv2D(filters, 3, padding='same', activation='relu')(x_in)
    x = Conv2D(filters, 3, padding='same')(x)
    if scaling:
        x = Lambda(lambda t: t * scaling)(x)
    x = Add()([x_in, x])
    return x


def upsample(x, scale, num_filters):
    def upsample_1(x, factor, **kwargs):
        """Sub-pixel convolution."""
        x = Conv2D(num_filters * (factor ** 2), 3, padding='same', **kwargs)(x)
        return Lambda(pixel_shuffle(scale=factor))(x)

    if scale == 2:
        x = upsample_1(x, 2, name='conv2d_1_scale_2')
    elif scale == 3:
        x = upsample_1(x, 3, name='conv2d_1_scale_3')
    elif scale == 4:
        x = upsample_1(x, 2, name='conv2d_1_scale_2')
        x = upsample_1(x, 2, name='conv2d_2_scale_2')

    return x


def pixel_shuffle(scale):
    return lambda x: tf.nn.depth_to_space(x, scale)


def normalize(x):
    return (x - DIV2K_RGB_MEAN) / 127.5


def denormalize(x):
    return x * 127.5 + DIV2K_RGB_MEAN


# model_edsr_x3 = edsr(scale=3, num_res_blocks=32, num_filters=256)
# model_edsr_x3_kai = edsr(scale=3, num_res_blocks=32, num_filters=256)
# model_edsr_x2 = edsr(scale=2, num_res_blocks=32, num_filters=256)
# model_edsr_x2_base = edsr(scale=2, num_res_blocks=16, num_filters=64)
# models_dict = {2: model_edsr_x2_base}


# models_dict = {2: model_edsr_x3,
#           3: model_edsr_x3,
#           3.1: model_edsr_x3_kai,
#           2.1: model_edsr_x2_base}


def load_weights():
    # x2_checkpoint_filepath
    # x2_base_checkpoint_filepath
    # x3_checkpoint_filepath
    # x3_kai_checkpoint_filepath
    # model_edsr_x2.load_weights(x2_checkpoint_filepath)
    # model_edsr_x2_base.load_weights(x2_base_checkpoint_filepath)
    # model_edsr_x3.load_weights(x3_checkpoint_filepath)
    # model_edsr_x3_kai.load_weights(x3_kai_checkpoint_filepath)
    pass


threading.Thread(target=load_weights).start()


def resolve_single(model, lr):
    return resolve(model, tf.expand_dims(lr, axis=0))[0]


def resolve(model, lr_batch):
    lr_batch = tf.cast(lr_batch, tf.float32)
    sr_batch = model(lr_batch)
    sr_batch = tf.clip_by_value(sr_batch, 0, 255)
    sr_batch = tf.round(sr_batch)
    sr_batch = tf.cast(sr_batch, tf.uint8)
    return sr_batch


def load_image(path):
    return np.array(Image.open(path))


def get_sr_path(obj: models.ProfileFeedItem):
    s = obj.upload_image.name.split('/')
    s[1] = 'SR'
    relative_file_path = '/'.join(s)
    return relative_file_path


def performSR(obj: models.ProfileFeedItem):
    lr = load_image(obj.upload_image)

    # sr = resolve_single(models_dict[obj.scaling_factor], lr)

    img = Image.fromarray(np.array(lr))

    relative_file_path = get_sr_path(obj)

    file_path = os.path.join(MEDIA_ROOT, relative_file_path)

    os.makedirs(os.path.dirname(file_path), exist_ok=True)

    img.save(file_path)

    obj.finished_processing = True
    obj.save()


job_queue = Queue()


# def consume(qu: Queue):
#     while True:
#         if not qu.empty():
#             print("waiting for a job")
#             obj = qu.get()
#             print("processing job")
#             performSR(obj)
#             print("finished processing")
#             time.sleep(1)
#
#
# p = threading.Thread(target=consume, args=(job_queue,))
# p.daemon = True
# p.start()
