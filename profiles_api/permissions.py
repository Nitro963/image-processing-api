from rest_framework import permissions


class UpdateOwnProfile(permissions.BasePermission):
    """Allow users to update their own profiles"""

    def has_object_permission(self, request, view, obj):
        """Check if the user is trying to edit his own profile"""
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.id == obj.id


class UploadFile(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return request.user.id == obj.user_profile.id
