import os

from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.conf import settings


class UserProfileManager(BaseUserManager):
    """Manger for user profile
    """

    def create_user(self, email, name, password=None):
        """Creates a new user profile

        :param email: user email
        :param name: user name
        :param password: user password
        :return: the created user
        """

        if not email:
            raise ValueError('User must have an email address')

        email = self.normalize_email(email)
        user = self.model(email=email, name=name)

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, name, password=None):
        """Creates a new super user profile

        :param email: user email
        :param name: user name
        :param password: user password
        :return: the created user
        """

        user = self.create_user(email, name, password)

        user.is_superuser = True
        user.is_staff = True

        user.save(using=self._db)

        return user


class UserProfile(AbstractBaseUser, PermissionsMixin):
    """Database model for users in the system.
    """
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def get_full_name(self):
        """
        :return: the full name of user
        """
        return self.name

    def get_short_name(self):
        """

        :return: the short name of user
        """
        return self.name

    def __str__(self):
        return self.email


def user_LR_directory_path(instance, filename):
    date = instance.created_on.date()
    time = instance.created_on.time()
    return f'user_{instance.user_profile.id}/LR/{date.year}/{date.month:0>2}/{date.day:0>2}/{str(time)}_{filename}'


class ProfileFeedItem(models.Model):
    user_profile = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    upload_image = models.ImageField(upload_to=user_LR_directory_path)
    scaling_factor = models.IntegerField(default=2)
    finished_processing = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.user_profile.name}: {os.path.basename(self.upload_image.name)}'

    def delete(self, using=None, keep_parents=False):
        models.Model.delete(self, using, keep_parents)

        lr_file_path = os.path.join(settings.MEDIA_ROOT, self.upload_image.name)
        os.remove(lr_file_path)

        s = self.upload_image.name.split('/')
        s[1] = 'SR'
        relative_file_path = '/'.join(s)
        sr_file_path = os.path.join(settings.MEDIA_ROOT, relative_file_path)
        os.remove(sr_file_path)

        lr_dir = os.path.dirname(lr_file_path)
        sr_dir = os.path.dirname(sr_file_path)
        try:
            os.removedirs(lr_dir)
        except OSError as e:
            print(e)
            pass

        try:
            os.removedirs(sr_dir)
        except OSError as e:
            print(e)
            pass

