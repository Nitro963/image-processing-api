import os
import time

import numpy as np

from PIL import Image
from profiles_api import models
from profiles_api import utility as utils
from processing_apis_project.settings import MEDIA_ROOT
# from processing_apis_project.celery import app


#@app.task
def performSR(obj: models.ProfileFeedItem):
    lr = utils.load_image(obj.upload_image)

    sr = utils.resolve_single(utils.models_dict[obj.scaling_factor], lr)

    img = Image.fromarray(np.array(sr))

    relative_file_path = utils.get_sr_path(obj)

    file_path = os.path.join(MEDIA_ROOT, relative_file_path)

    os.makedirs(os.path.dirname(file_path), exist_ok=True)

    img.save(file_path)

    obj.finished_processing = True
    obj.save()


# @app.task
# def test_tasks():
#     print("sleeping for 4 sec")
#     time.sleep(4)
#     print("I am awake")
