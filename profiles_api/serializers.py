import os

from rest_framework import serializers

from . import models


class HelloSerializer(serializers.Serializer):
    """Serializes a name field for testing purposes
    """

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    name = serializers.CharField(max_length=10)


class UserProfileSerializer(serializers.ModelSerializer):
    """Serializes a user profile object"""

    class Meta:
        model = models.UserProfile
        fields = ('id', 'email', 'name', 'password')

        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {
                    'input_type': 'password'
                }
            }
        }

    def create(self, validated_data):
        """Create and return a new user"""
        return models.UserProfile.objects.create_user(**validated_data)


class ProfileFeedItemSerializer(serializers.ModelSerializer):
    """Serializes profile feed item"""

    class Meta:
        model = models.ProfileFeedItem
        fields = ('id', 'user_profile', 'upload_image', 'created_on',  'scaling_factor', 'finished_processing')
        extra_kwargs = {'user_profile': {'read_only': True},
                        'finished_processing': {'read_only': True}}

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        request = self.context.get('request', None)
        ret['upload_image'] = request.build_absolute_uri(os.path.join(f'{instance.id}/', instance.upload_image.name))
        ret['created_on'] = str(instance.created_on.date())
        return ret

    # def validate_scaling_factor(self, value):
    #     if value not in [2, 3, 4]:
    #         raise serializers.ValidationError(f'This field must be one of 2, 3 or 4')
    #     return value
