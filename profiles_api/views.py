import os

from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.authentication import TokenAuthentication
from rest_framework import filters
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from django.http import FileResponse

from processing_apis_project.settings import MEDIA_ROOT

from profiles_api import serializers
from profiles_api import models
from profiles_api import permissions
from profiles_api import utility as utils
from profiles_api import tasks


class HelloAPIView(APIView):
    """Test Api view
    """
    serializer_class = serializers.HelloSerializer

    # parser_classes = (MultiPartParser, FormParser)

    def get(self, request, format=None):
        """Return a list of APIView features
        """

        an_apiview = [
            'Uses HTTP methods as functions (get, post, patch, put, delete',
            'Is similar to a traditional Django view',
            'Gives you the most control over your application logic',
            'Is mapped manually to URLs',
        ]
        return Response({'message': 'Hello, World!', 'an_apiview': an_apiview})

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            name = serializer.validated_data.get('name')
            message = f'Hello {name}'
            return Response({'message': message})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfilesApi(viewsets.ModelViewSet):
    """Handle creating and updating profiles"""

    serializer_class = serializers.UserProfileSerializer
    queryset = models.UserProfile.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.UpdateOwnProfile,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'email',)


class UserLoginApiView(ObtainAuthToken):
    """Handle creating user authentication token"""

    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


def get_celery_worker_status():
    ERROR_KEY = "ERROR"
    try:
        from celery.task.control import inspect
        insp = inspect()
        d = insp.stats()
        if not d:
            d = { ERROR_KEY: 'No running Celery workers were found.' }
    except IOError as e:
        from errno import errorcode
        msg = "Error connecting to the backend: " + str(e)
        if len(e.args) > 0 and errorcode.get(e.args[0]) == 'ECONNREFUSED':
            msg += ' Check that the RabbitMQ server is running.'
        d = { ERROR_KEY: msg }
    except ImportError as e:
        d = { ERROR_KEY: str(e)}
    return d


class UserProfileFeedViewSet(mixins.CreateModelMixin,
                             mixins.RetrieveModelMixin,
                             mixins.DestroyModelMixin,
                             mixins.ListModelMixin,
                             viewsets.GenericViewSet):
    """Handle creating tasks and downloading results"""

    serializer_class = serializers.ProfileFeedItemSerializer
    authentication_classes = (TokenAuthentication,)
    queryset = models.ProfileFeedItem.objects.all()
    permission_classes = (IsAuthenticated, permissions.UploadFile)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('upload_image', 'created_on')

    def get_queryset(self):
        user = self.request.user
        return models.ProfileFeedItem.objects.filter(user_profile=user)

    def perform_create(self, serializer):
        obj = serializer.save(user_profile=self.request.user)
        # utils.job_queue.put(obj)
        utils.performSR(obj)

    @action(detail=False, url_path='check')
    def check(self, request: Request, pk=None):
        # tasks.test_tasks.delay()
        pk = request.query_params.get('task_id', None)
        if pk is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        obj: models.ProfileFeedItem = models.ProfileFeedItem.objects.get(pk=pk)

        self.check_object_permissions(request, obj)

        if not obj.finished_processing:
            return Response({'state':False, 'message': 'please wait for a while.'})

        relative_file_path = utils.get_sr_path(obj)
        uri = request.build_absolute_uri(relative_file_path)
        uri = str.replace(uri, 'check', f'{obj.pk}', 1)
        return Response({'state': True, 'message': 'done processing.', 'url': uri})

    @action(detail=True, url_path='user_(\\d+)'
                                  '/SR'
                                  '/(\\d{4})'
                                  '/([0-1]?\\d)'
                                  '/((0?\\d)|([1-2]\\d)|(3[0-1]))'
                                  '/(\\w|\\s|\\.|\\-)+\\.(jpg|png)')
    def download(self, request: Request, pk=None):
        obj = self.get_object()

        if not obj.finished_processing:
            return Response(status=status.HTTP_404_NOT_FOUND)

        relative_file_path = utils.get_sr_path(obj)
        file_path = os.path.join(MEDIA_ROOT, relative_file_path)
        if os.path.exists(file_path):
            return FileResponse(open(file_path, 'rb'), as_attachment=False)

        return Response(status=status.HTTP_404_NOT_FOUND)

    @action(detail=True, url_path='user_(\\d+)'
                                  '/LR'
                                  '/(\\d{4})'
                                  '/([0-1]?\\d)'
                                  '/((0?\\d)|([1-2]\\d)|(3[0-1]))'
                                  '/(\\w|\\s|\\.|\\-)+\\.(jpg|png)')
    def download_origin(self, request: Request, pk=None):
        obj = self.get_object()
        relative_file_path = obj.upload_image.name
        file_path = os.path.join(MEDIA_ROOT, relative_file_path)
        if os.path.exists(file_path):
            return FileResponse(open(file_path, 'rb'), as_attachment=False)
        return Response(status=status.HTTP_404_NOT_FOUND)

